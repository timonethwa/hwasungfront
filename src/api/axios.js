import axios from 'axios';

// url 설정 
const createInstance = () => {
    const instance = axios.create({
      // baseURL: process.env.VUE_APP_API_URL, 
      withCredentials: true,
      loading: true,
      timeout: 10000,
    });
     return instance;
}

const api = createInstance();

export default api;