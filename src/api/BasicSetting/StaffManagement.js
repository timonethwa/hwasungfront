import api from "@/api/axios";

const StaffManagement = {
  readStaffManagementInfo: () => {
    return api.get("/staffManagement/read");
  },
  createStaffManagementInfo: (StaffStateColorInfo) => {
    return api.post("/staffManagement/create", StaffStateColorInfo);
  },
  deleteStaffManagementInfo: (StaffStateColorInfo) => {
    return api.post("/staffManagement/delete", StaffStateColorInfo);
  },
  updateStaffManagementInfo: (StaffStateColorInfo) => {
    return api.put("/staffManagement/update", StaffStateColorInfo);
  },
};

export default StaffManagement;
