import api from "@/api/axios";

const FormOutput = {
  readFormOutputInfo: (FormOutputInfo) => {
    return api.post("/formOutput/read", FormOutputInfo);
  },
};

export default FormOutput;
