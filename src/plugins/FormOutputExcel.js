
const FormOutputExcel = {
  chiefOfTheIlgyeExcel: function (data, firstDate, lastDate) {
    const tempStyle = `
            <style>
                .xl67
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:none;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:1.0pt solid windowtext;}
                .xl68
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:none;
                    border-right:1.0pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl69
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:none;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl70
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:none;
                    border-right:none;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;}
                .xl71
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:none;}
                .xl72
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:2.0pt double windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl73
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:2.0pt double windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:1.0pt solid windowtext;}
                .xl74
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:2.0pt double windowtext;
                    border-right:1.0pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl75
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:.5pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:1.0pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl76
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:.5pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:1.0pt solid windowtext;
                    border-left:1.0pt solid windowtext;}
                .xl77
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    border-top:.5pt solid windowtext;
                    border-right:1.0pt solid windowtext;
                    border-bottom:1.0pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl78
                    {mso-style-parent:style16;
                    vertical-align:middle;}
                .xl79
                    {mso-style-parent:style16;
                    font-size:9.0pt;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    text-align:right;
                    vertical-align:middle;}
                .xl80
                    {mso-style-parent:style16;
                    font-size:9.0pt;
                    text-align:right;
                    vertical-align:middle;}
                .xl81
                    {mso-style-parent:style16;
                    font-size:28.0pt;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    vertical-align:middle;}
                .xl82
                    {mso-style-parent:style16;
                    font-size:28.0pt;
                    vertical-align:middle;}
                .xl83
                    {mso-style-parent:style16;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    text-align:center;
                    vertical-align:middle;}
                .xl84
                    {mso-style-parent:style16;
                    text-align:center;
                    vertical-align:middle;}
                .xl85
                    {mso-style-parent:style16;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    text-align:center;
                    vertical-align:middle;
                    border:1.0pt solid windowtext;}
                .xl86
                    {mso-style-parent:style16;
                    text-align:center;
                    vertical-align:middle;
                    border:1.0pt solid windowtext;}
                .xl87
                    {mso-style-parent:style0;
                    border-top:none;
                    border-right:none;
                    border-bottom:none;
                    border-left:1.0pt solid windowtext;}
                .xl88
                    {mso-style-parent:style16;
                    font-size:12.0pt;
                    mso-number-format:"0_);[Red](0)";
                    text-align:center;}
                .xl89
                    {mso-style-parent:style16;
                    text-align:center;
                    vertical-align:middle;
                    border-top:.5pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:1.0pt solid windowtext;}
                .xl90
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:.5pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:.5pt solid windowtext;}
                .xl91
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:.5pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:.5pt solid windowtext;}
                .xl92
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:.5pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:none;}
                .xl93
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:.5pt solid windowtext;
                    border-right:1.0pt solid windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:.5pt solid windowtext;}
                .xl94
                    {mso-style-parent:style0;
                    text-align:center;}
                .xl95
                    {mso-style-parent:style16;
                    font-size:26.0pt;
                    text-align:center;
                    vertical-align:middle;}
                .xl96
                    {mso-style-parent:style16;
                    font-size:26.0pt;
                    text-align:center;
                    vertical-align:middle;
                    border-top:none;
                    border-right:none;
                    border-bottom:2.0pt double windowtext;
                    border-left:none;}
                .xl97
                    {mso-style-parent:style16;
                    text-align:center;
                    vertical-align:middle;
                    border-top:1.0pt solid windowtext;
                    border-right:1.0pt solid windowtext;
                    border-bottom:none;
                    border-left:1.0pt solid windowtext;}
                .xl98
                    {mso-style-parent:style16;
                    text-align:center;
                    vertical-align:middle;
                    border-top:none;
                    border-right:1.0pt solid windowtext;
                    border-bottom:none;
                    border-left:1.0pt solid windowtext;}
                .xl99
                    {mso-style-parent:style16;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    text-align:center;
                    vertical-align:middle;
                    border-top:1.0pt solid windowtext;
                    border-right:1.0pt solid windowtext;
                    border-bottom:none;
                    border-left:1.0pt solid windowtext;}
                .xl100
                    {mso-style-parent:style16;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    text-align:center;
                    vertical-align:middle;
                    border-top:none;
                    border-right:1.0pt solid windowtext;
                    border-bottom:none;
                    border-left:1.0pt solid windowtext;}
                .xl101
                    {mso-style-parent:style16;
                    mso-number-format:"#,##0;[Red]-#,##0";
                    text-align:center;
                    vertical-align:middle;
                    border-top:none;
                    border-right:none;
                    border-bottom:none;
                    border-left:1.0pt solid windowtext;}
                .xl102
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:middle;
                    border-top:1.0pt solid windowtext;
                    border-right:none;
                    border-bottom:none;
                    border-left:none;}
                .xl103
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:middle;
                    border-top:1.0pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:none;}
                .xl104
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:middle;
                    border-top:none;
                    border-right:none;
                    border-bottom:2.0pt double windowtext;
                    border-left:none;}
                .xl105
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:middle;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:none;}
                .xl106
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:1.0pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:1.0pt solid windowtext;}
                .xl107
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:1.0pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl108
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:1.0pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl109
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:1.0pt solid windowtext;
                    border-right:.5pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:none;}
                .xl110
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:1.0pt solid windowtext;
                    border-right:1.0pt solid windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:.5pt solid windowtext;}
                .xl111
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:2.0pt double windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;}
                .xl112
                    {mso-style-parent:style0;
                    text-align:center;
                    border-top:.5pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:1.0pt solid windowtext;
                    border-left:2.0pt double windowtext;}
            </style>    
        `;

    const tempHeader = `
            <tr height=22 style='height:16.5pt'>
                <td height=22 width=96 style='height:16.5pt;width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
                <td width=96 style='width:72pt'></td>
            </tr>
        `;

    // 날짜 별로 데이터 parsing 작업
    // ex) 날짜, 이월금액, 데이터
    var dateArray = new Array();

    for (var x = 0; x < data.length; x++) {
        var dataArray = new Array();
        var dateObject = new Object();
        var dataObject = new Object();
        // date parsing 작업
        if (dateArray.length == 0) {
            dataObject = data[x];
            dataArray.push(dataObject);
            dateObject.transaction_date = data[x].transaction_date;
            dateObject.bank_balance = data[x].bank_balance;
            dateObject.data = dataArray;
            dateArray.push(dateObject);
        } else {
            var dateDate = data[x].transaction_date;
            for (var y = 0; y < dateArray.length; y++) {
            if (dateArray[y].transaction_date == dateDate) {
                var datasObject = new Object();
                datasObject = data[x];
                dateArray[y].data.push(datasObject);
                dateArray[y].bank_balance = data[x].bank_balance;
                dateDate = "";
            }
            }
            if (dateDate != "") {
            dateObject.transaction_date = dateDate;
            dataObject = data[x];
            dateObject.bank_balance = data[x].bank_balance;
            dataArray.push(dataObject);
            dateObject.data = dataArray;
            dateArray.push(dateObject);
            }
        }
    }

    var temp = ``;
    // 이월 금액
    var moneyFlag = true;
    var recordMoney;
    for (var i = 0; i < dateArray.length; i++) {
      const dataArrayInfo = dateArray[i].data;
      const dataFormrt =
        dateArray[i].transaction_date.substring(0, 4) +
        "년 " +
        dateArray[i].transaction_date.substring(5, 7) +
        "월 " +
        dateArray[i].transaction_date.substring(8) +
        "일";
      if (moneyFlag) {
        // 초기 이월 금액  금액 설정
        recordMoney = data[i].bank_balance;
        moneyFlag = false;
      }
      var moneyIndayTotalMoney = 0;
      var moneyOutdayTotalMoney = 0;
      temp += `   
                <tr height=22 style='height:16.5pt'> 
                    <td height=22 class=xl78 style='height:16.5pt'></td>
                    <td colspan=6 rowspan=2 class=xl95 style='border-bottom:2.0pt double black'>일<span style='mso-spacerun:yes'>&nbsp;&nbsp; </span>계<span style='mso-spacerun:yes'>&nbsp;&nbsp; </span>표</td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl78 style='height:16.5pt'></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                </tr>
                <tr height=55 style='height:41.25pt'>
                    <td height=55 class=xl78 style='height:41.25pt'></td>
                    <td class=xl78></td>
                    <td class=xl79></td>
                    <td class=xl79></td>
                    <td class=xl80></td>
                    <td class=xl80></td>
                    <td class=xl81></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                </tr>
                <tr height=55 style='height:41.25pt'>
                    <td height=55 class=xl78 style='height:41.25pt'></td>
                    <td class=xl78></td>
                    <td class=xl81></td>
                    <td class=xl81></td>
                    <td class=xl82></td>
                    <td class=xl82></td>
                    <td class=xl81></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl78 style='height:16.5pt'></td>
                    <td class=xl78></td>
                    <td class=xl83></td>
                    <td class=xl83></td>
                    <td class=xl84></td>
                    <td class=xl85>취 급 자</td>
                    <td class=xl86 style='border-left:none'>간<span style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; </span>사</td>
                    <td class=xl85 style='border-left:none'>책 임 자</td>
                    <td></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl78 style='height:16.5pt'>입금표</td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td rowspan=4 class=xl99 style='border-top:none'></td>
                    <td rowspan=4 class=xl97 style='border-top:none'></td>
                    <td rowspan=4 class=xl99 style='border-top:none'></td>
                    <td></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl78 style='height:16.5pt'>출금표</td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl78 style='height:16.5pt'>대체전표</td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl78></td>
                    <td class=xl87></td>
                </tr>
                <tr height=23 style='height:17.25pt'>
                    <td height=23 class=xl78 style='height:17.25pt'></td>
                    <td class=xl78></td>
                    <td class=xl88></td>
                    <td colspan=2>${dataFormrt}</td>
                    <td class=xl87></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td colspan=3 height=22 class=xl106 style='border-right:2.0pt double black; height:16.5pt'>차변</td>
                    <td colspan=2 rowspan=2 class=xl102 style='border-right:2.0pt double black; border-bottom:2.0pt double black'>과목</td>
                    <td colspan=3 class=xl109 style='border-right:1.0pt solid black'>대변</td>
                    <td></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl89 style='height:16.5pt;border-top:none'>합계</td>
                    <td class=xl90 style='border-top:none;border-left:none'>대체</td>
                    <td class=xl91 style='border-top:none;border-left:none'>출금</td>
                    <td class=xl92 style='border-top:none'>입금</td>
                    <td class=xl90 style='border-top:none;border-left:none'>대체</td>
                    <td class=xl93 style='border-top:none;border-left:none'>합계</td>
                    <td class=xl94></td>
                </tr>
            `;

      for (var q = 0; q < dataArrayInfo.length; q++) {
        if (i == 0) {
          temp += ` 
                    <tr height=22 style='height:16.5pt'>
                        <td height=22 class=xl67 align=right style='height:16.5pt'>${dataArrayInfo[q].money_in}</td>
                        <td class=xl69 style='border-left:none'></td>
                        <td class=xl69 align=right style='border-left:none'>${dataArrayInfo[q].money_in}</td>
                        <td class=xl70>${dataArrayInfo[q].transaction_name}</td>
                        <td class=xl71>${dataArrayInfo[q].username}</td>
                        <td class=xl69 align=right>${dataArrayInfo[q].money_in}</td>
                        <td class=xl69 style='border-left:none'></td>
                        <td class=xl68 align=right style='border-left:none'>${dataArrayInfo[q].bank_balance}</td>
                        <td></td>
                    </tr>
                `;
          moneyIndayTotalMoney += dataArrayInfo[q].bank_balance;
          moneyOutdayTotalMoney += dataArrayInfo[q].bank_balance;
        } else if (dataArrayInfo[q].money_in != 0 && i != 0) {
          //대변
          temp += ` 
                    <tr height=22 style='height:16.5pt'>
                        <td height=22 class=xl67 align=right style='height:16.5pt'></td>
                        <td class=xl69 style='border-left:none'></td>
                        <td class=xl69 align=right style='border-left:none'></td>
                        <td class=xl70>${dataArrayInfo[q].transaction_name}</td>
                        <td class=xl71>${dataArrayInfo[q].username}</td>
                        <td class=xl69 align=right>${dataArrayInfo[q].money_in}</td>
                        <td class=xl69 style='border-left:none'></td>
                        <td class=xl68 align=right style='border-left:none'>${dataArrayInfo[q].money_in}</td>
                        <td></td>
                    </tr>
                `;
          moneyIndayTotalMoney += dataArrayInfo[q].money_in;
        } else if (dataArrayInfo[q].money_out != 0 && i != 0) {
          //차변
          temp += ` 
                    <tr height=22 style='height:16.5pt'>
                        <td height=22 class=xl67 align=right style='height:16.5pt'>${dataArrayInfo[q].money_out}</td>
                        <td class=xl69 style='border-left:none'></td>
                        <td class=xl69 align=right style='border-left:none'>${dataArrayInfo[q].money_out}</td>
                        <td class=xl70>${dataArrayInfo[q].transaction_name}</td>
                        <td class=xl71>${dataArrayInfo[q].username}</td>
                        <td class=xl69 align=right></td>
                        <td class=xl69 style='border-left:none'></td>
                        <td class=xl68 align=right style='border-left:none'></td>
                        <td></td>
                    </tr>
                `;
          moneyOutdayTotalMoney += dataArrayInfo[q].money_out;
        }
      }

      temp += `
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl73 align=right style='height:16.5pt'>${moneyOutdayTotalMoney}</td>
                    <td class=xl72 style='border-left:none'></td>
                    <td class=xl72 align=right style='border-left:none'>${moneyOutdayTotalMoney}</td>
                    <td colspan=2 class=xl111>일계</td>
                    <td class=xl72 align=right>${moneyIndayTotalMoney}</td>
                    <td class=xl72 style='border-left:none'></td>
                    <td class=xl74 align=right style='border-left:none'>${moneyIndayTotalMoney}</td>
                    <td></td>
                </tr >    
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl67 align=right style='height:16.5pt'>${recordMoney}</td>
                    <td class=xl69 style='border-left:none'></td>
                    <td class=xl69 align=right style='border-left:none'>${recordMoney}</td>
                    <td colspan=2 class=xl70 style='border-right:2.0pt double black'>금월잔액/전월잔액</td>
                    <td class=xl69 align=right>${recordMoney}</td>
                    <td class=xl69 style='border-left:none'></td>
                    <td class=xl68 align=right style='border-left:none'>${recordMoney}</td>
                    <td></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl76 align=right style='height:16.5pt;border-top:none'>${dateArray[i].bank_balance}</td>
                    <td class=xl75 style='border-top:none;border-left:none'></td>
                    <td class=xl75 align=right style='border-top:none;border-left:none'>${dateArray[i].bank_balance}</td>
                    <td colspan=2 class=xl112>합계</td>
                    <td class=xl75 align=right style='border-top:none'>${dateArray[i].bank_balance}</td>
                    <td class=xl75 style='border-top:none;border-left:none'></td>
                    <td class=xl77 align=right style='border-top:none;border-left:none'>${dateArray[i].bank_balance}</td>
                    <td></td>
                </tr>
                <tr height=66 style='height:49.5pt;mso-xlrowspan:3'>
                    <td height=66 colspan=9 style='height:49.5pt;mso-ignore:colspan'></td>
                </tr>
            `;
      // 나중 이월 금액   금액 설정
      recordMoney = dateArray[i].bank_balance;
    }

        let tab_text =
        '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
        tab_text +=
        '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">';
        tab_text += "<xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>";
        tab_text += "<x:Name>일계정원장</x:Name>";
        tab_text +=
        "<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>";
        tab_text += "</x:ExcelWorksheets></x:ExcelWorkbook></xml>";
        tab_text += tempStyle;
        tab_text += "</head><body link='#0563C1' vlink='#954F72'>";
        tab_text +=
        "<table border=0 cellpadding=0 cellspacing=0 width=864 style='border-collapse:collapse;table-layout:fixed;width:648pt'>";
        tab_text += "<col width=96 span=9 style='width:72pt'>";
        tab_text += tempHeader;
        tab_text += temp;
        tab_text += "</table></body></html>";
        console.log(firstDate);
        const fileName = "일계장원장 " + firstDate + "~" + lastDate + ".xls";
        const a_tag = document.createElement("a");
        const blob = new Blob([tab_text], {
        type: "application/vnd.ms-excel;charset=utf-8;",
        });
        a_tag.href = window.URL.createObjectURL(blob);
        a_tag.download = fileName;
        a_tag.click();
    },
    cashRegisterExcel: function (data, firstDate, lastDate) {
      const tempStyle = `
            <style>    
                .xl65
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl66
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl67
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    text-align:center;
                    vertical-align:bottom;
                    border-top:none;
                    border-right:none;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl68
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    text-align:center;
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:none;}
                .xl69
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;
                    background:#969696;
                    mso-pattern:black none;}
                .xl70
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    vertical-align:bottom;
                    border-top:none;
                    border-right:none;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;
                    background:#969696;
                    mso-pattern:black none;}
                .xl71
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:none;
                    background:#969696;
                    mso-pattern:black none;}
                .xl72
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:.5pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl73
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:.5pt solid windowtext;
                    border-right:none;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl74
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:.5pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:none;}
                .xl75
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    vertical-align:bottom;
                    border-top:.5pt solid windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl76
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;}
                .xl77
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:none;
                    border-right:none;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;}
                .xl78
                    {mso-style-parent:style0;
                    text-align:center;
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:none;}
                .xl79
                    {mso-style-parent:style0;
                    mso-number-format:"#,##0";
                    vertical-align:bottom;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:.5pt solid windowtext;
                    border-left:2.0pt double windowtext;}
                .xl80
                    {mso-style-parent:style0;
                    font-weight:700;
                    text-align:center;
                    border-top:2.0pt double windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl81
                    {mso-style-parent:style0;
                    font-weight:700;
                    text-align:center;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:2.0pt double windowtext;}
                .xl82
                    {mso-style-parent:style0;
                    font-weight:700;
                    text-align:center;
                    border-top:2.0pt double windowtext;
                    border-right:none;
                    border-bottom:none;
                    border-left:2.0pt double windowtext;}
                .xl83
                    {mso-style-parent:style0;
                    font-weight:700;
                    text-align:center;
                    border-top:none;
                    border-right:none;
                    border-bottom:2.0pt double windowtext;
                    border-left:2.0pt double windowtext;}
                .xl84
                    {mso-style-parent:style0;
                    font-weight:700;
                    text-align:center;
                    border-top:2.0pt double windowtext;
                    border-right:2.0pt double windowtext;
                    border-bottom:none;
                    border-left:none;}
                .xl85
                    {mso-style-parent:style0;
                    font-weight:700;
                    text-align:center;
                    border-top:none;
                    border-right:2.0pt double windowtext;
                    border-bottom:2.0pt double windowtext;
                    border-left:none;
            </style>         
        `;

        const tempHeader = `
            <col width=45 span=2 style='mso-width-source:userset;mso-width-alt:1440; width:34pt'>
            <col width=125 span=2 style='mso-width-source:userset;mso-width-alt:4000; width:94pt'>
            <col width=118 span=2 style='mso-width-source:userset;mso-width-alt:3776; width:89pt'>
            <col width=22 style='mso-width-source:userset;mso-width-alt:704;width:17pt'>
            <col width=118 style='mso-width-source:userset;mso-width-alt:3776;width:89pt'>
            <tr height=23 style='height:17.25pt'>
                <td rowspan=2 height=46 class=xl82 width=45 style='border-bottom:2.0pt double black; height:34.5pt;width:34pt'>월</td>
                <td rowspan=2 class=xl84 width=45 style='border-bottom:2.0pt double black; width:34pt'>일</td>
                <td colspan=2 rowspan=2 class=xl82 width=250 style='border-right:2.0pt double black; border-bottom:2.0pt double black;width:188pt'>적요</td>
                <td rowspan=2 class=xl80 width=118 style='border-bottom:2.0pt double black; width:89pt'>차변</td>
                <td rowspan=2 class=xl80 width=118 style='border-bottom:2.0pt double black; width:89pt'>대변</td>
                <td rowspan=2 class=xl80 width=22 style='border-bottom:2.0pt double black; width:17pt'></td>
                <td rowspan=2 class=xl80 width=118 style='border-bottom:2.0pt double black; width:89pt'>잔액</td>
            </tr>
            <tr height=23 style='height:17.25pt'>
            </tr>      
        `;

        // 날짜 별로 데이터 parsing 작업
        // ex) 날짜, 이월금액, 데이터
        var dateArray = new Array();

        for (var x = 0; x < data.length; x++) {
            var dataArray = new Array();
            var dateObject = new Object();
            var dataObject = new Object();
            // date parsing 작업
            if (dateArray.length == 0) {
                dataObject = data[x];
                dataArray.push(dataObject);
                dateObject.transaction_date = data[x].transaction_date;
                dateObject.bank_balance = data[x].bank_balance;
                dateObject.data = dataArray;
                dateArray.push(dateObject);
            } else {
                var dateDate = data[x].transaction_date;
                for (var y = 0; y < dateArray.length; y++) {
                    if (dateArray[y].transaction_date == dateDate) {
                    var datasObject = new Object();
                    datasObject = data[x];
                    dateArray[y].data.push(datasObject);
                    dateArray[y].bank_balance = data[x].bank_balance;
                    dateDate = "";
                    }
                }
                if (dateDate != "") {
                    dateObject.transaction_date = dateDate;
                    dataObject = data[x];
                    dateObject.bank_balance = data[x].bank_balance;
                    dataArray.push(dataObject);
                    dateObject.data = dataArray;
                    dateArray.push(dateObject);
                }
            }
        }
        var temp = ``;
        var moneyInTotalMoney = 0;
         var moneyOutTotalMoney = 0;
        for (var i = 0; i < dateArray.length; i++){
            const dataArrayInfo = dateArray[i].data;
            const dataMonthFormrt = dateArray[i].transaction_date.substring(5, 7);
            const dataDayFormrt = dateArray[i].transaction_date.substring(8);
            var moneyIndayTotalMoney = 0;
            var moneyOutdayTotalMoney = 0;

            for (var q = 0; q < dataArrayInfo.length; q++){
                temp += `
                    <tr height=22 style='height:16.5pt'>
                        <td height=22 class=xl67 style='height:16.5pt'>${dataMonthFormrt}</td>
                        <td class=xl68>${dataDayFormrt}</td>
                        <td class=xl67 style='border-left:none'>${dataArrayInfo[q].transaction_name}</td>
                        <td class=xl68>${dataArrayInfo[q].username}</td>
                        <td class=xl66 style='border-left:none'>${dataArrayInfo[q].money_out == 0 ? '' : dataArrayInfo[q].money_out}</td>
                        <td class=xl66 align=right style='border-left:none'>${dataArrayInfo[q].money_in == 0 ? '' : dataArrayInfo[q].money_in}</td>
                        <td class=xl65 style='border-left:none'></td>
                        <td class=xl66 style='border-left:none'></td>
                    </tr>
                `;
                if (dataArrayInfo[q].money_out != 0) {
                    moneyOutTotalMoney += dataArrayInfo[q].money_out
                    moneyOutdayTotalMoney += dataArrayInfo[q].money_out
                }
                if (dataArrayInfo[q].money_in != 0) {
                    moneyInTotalMoney += dataArrayInfo[q].money_in
                    moneyIndayTotalMoney += dataArrayInfo[q].money_in;
                }
            }

            temp += `
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl70 style='height:16.5pt'></td>
                    <td class=xl71></td>
                    <td class=xl70 style='border-left:none'></td>
                    <td class=xl71></td>
                    <td class=xl69 style='border-left:none'></td>
                    <td class=xl69 style='border-left:none'></td>
                    <td class=xl69 style='border-left:none'></td>
                    <td class=xl69 style='border-left:none'></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl73 style='height:16.5pt;border-top:none'></td>
                    <td class=xl74 style='border-top:none'></td>
                    <td class=xl73 style='border-top:none;border-left:none'></td>
                    <td class=xl74 style='border-top:none'>일 계</td>
                    <td class=xl75 align=right style='border-top:none;border-left:none'>${moneyOutdayTotalMoney}</td>
                    <td class=xl75 align=right style='border-top:none;border-left:none'>${moneyIndayTotalMoney}</td>
                    <td class=xl72 style='border-top:none;border-left:none'></td>
                    <td class=xl72 style='border-top:none;border-left:none'></td>
                </tr>
                <tr height=22 style='height:16.5pt'>
                    <td height=22 class=xl77 style='height:16.5pt'></td>
                    <td class=xl78></td>
                    <td class=xl77 style='border-left:none'></td>
                    <td class=xl78>누 계</td>
                    <td class=xl79 align=right style='border-left:none'>${moneyOutTotalMoney}</td>
                    <td class=xl79 align=right style='border-left:none'>${moneyInTotalMoney}</td>
                    <td class=xl76 style='border-left:none'></td>
                    <td class=xl79 align=right style='border-left:none'>${dateArray[i].bank_balance}</td>
                </tr>  
            `;
            
        }
        let tab_text =
            '<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
        tab_text +=
            '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">';
        tab_text += "<xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>";
        tab_text += "<x:Name>현금출납부</x:Name>";
        tab_text +=
            "<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>";
        tab_text += "</x:ExcelWorksheets></x:ExcelWorkbook></xml>";
        tab_text += tempStyle;
        tab_text += "</head><body link='#0563C1' vlink='#954F72'>";
        tab_text +=
            "<table border=0 cellpadding=0 cellspacing=0 width=716 style='border-collapse:collapse;table-layout:fixed;width:540pt'>";
        tab_text += tempHeader;
        tab_text += temp;
        tab_text += "</table></body></html>";
        const fileName = "현금출납부 " + firstDate + "~" + lastDate + ".xls";
        const a_tag = document.createElement("a");
        const blob = new Blob([tab_text], {
            type: "application/vnd.ms-excel;charset=utf-8;",
        });
        a_tag.href = window.URL.createObjectURL(blob);
        a_tag.download = fileName;
        a_tag.click();
    },
};

module.exports = FormOutputExcel;
