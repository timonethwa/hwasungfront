import api from "@/api/axios";

const DepartmentManagement = {
    readDepartmentHistoryInfo: (DepartmentInfo) => {
        return api.post("/departmentHistory/read", DepartmentInfo);
    },
};

export default DepartmentManagement;
