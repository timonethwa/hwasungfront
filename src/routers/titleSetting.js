import vue from 'vue';

const titleSetting = (to) => {
    const title = to.meta.title === undefined ? '화성 시청' : to.meta.title;
    vue.nextTick(() => {
        document.title = title;
    })
}

export default titleSetting