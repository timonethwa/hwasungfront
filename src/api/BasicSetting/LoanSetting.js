import api from "@/api/axios";

const LoanSetting = {
  readIdLoanSettingInfo: () => {
    return api.get("/loanSetting/read");
  },
  createIdLoanSettingInfo: (LoanSettingInfo) => {
    return api.post("/loanSetting/create", LoanSettingInfo);
  },
};

export default LoanSetting;
