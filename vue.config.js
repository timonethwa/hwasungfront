// node의 path 모듈을 가져옵니다.
const path = require('path');

module.exports = {
  devServer: {
    proxy: {
      "^/": {
        target: process.env.VUE_APP_API_URL,
        ws: true,
        changeOrigin: true,
      },
    },
    historyApiFallback: true,
  },
  chainWebpack: (config) => {
    config.resolve.alias.set("@", path.resolve(__dirname, "src/"));
  },
  runtimeCompiler: true,
  outputDir: "../back-end/public",
  transpileDependencies: ["vuetify"],
};
