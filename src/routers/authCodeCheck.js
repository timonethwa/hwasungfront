import store from "@/store";

const authCodeCheck = (to, from, next) => {
    const _store = store;
    if (to.meta.requireLogin) {
        if (_store.getters.isLogin) {
            next();
        } else {
            next({
                path: "/",
            });
        }
    } else {
        next();
    }
};

export default authCodeCheck;