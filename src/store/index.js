import Vue from "vue";
import Vuex from "vuex";
import authStore from "@/store/authStore";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: {
    authStore,
  },
  plugins: [
    createPersistedState({
      paths: ["authStore"],
    }),
  ],
});
