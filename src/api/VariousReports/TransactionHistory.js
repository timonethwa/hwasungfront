import api from "@/api/axios";

const TransactionHistory = {
  readTransactionHistoryInfo: (TransactionHistoryInfo) => {
    return api.post("/transactionHistory/read", TransactionHistoryInfo);
  },
};

export default TransactionHistory;
