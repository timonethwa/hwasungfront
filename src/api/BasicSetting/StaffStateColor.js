import api from "@/api/axios";

const StaffStateColor = {
  readStaffStateColorInfo: () => {
    return api.get("/staffStateColor/read");
  },
  createStaffStateColorInfo: (StaffStateColorInfo) => {
    return api.post("/staffStateColor/create", StaffStateColorInfo);
  },
  deleteStaffStateColorInfo: (StaffStateColorInfo) => {
    return api.post("/staffStateColor/delete", StaffStateColorInfo);
  },
  updateStaffStateColorInfo: (StaffStateColorInfo) => {
    return api.put("/staffStateColor/update", StaffStateColorInfo);
  },
};

export default StaffStateColor;
