import Vue from "vue";
import VueRouter from "vue-router";
// import authCodeCheck from "./authCodeCheck";
import titleSetting from "./titleSetting";

Vue.use(VueRouter);

const route = [
  {
    path: "/",
    name: "Login",
    component: () => import("@/views/Login"),
    meta: { title: "화성시청" },
  },
  {
    path: "/Main",
    name: "Main",
    component: () => import("@/views/Main"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/Department",
    name: "Department",
    component: () => import("@/views/BasicSetting/DepartmentManagement"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/Staff",
    name: "Staff",
    component: () => import("@/views/BasicSetting/StaffManagement"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/StaffColor",
    name: "StaffColor",
    component: () => import("@/views/BasicSetting/StaffStateColor"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/LoanSetting",
    name: "LoanSetting",
    component: () => import("@/views/BasicSetting/LoanSetting"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/IdManagement",
    name: "IdManagement",
    component: () => import("@/views/BasicSetting/IdManagement"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/DepartHistory/:depart_id",
    name: "DepartHistory",
    props: true,
    component: () => import("@/views/BasicSetting/DepartHistory"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/FormOutput",
    name: "FormOutput",
    props: true,
    component: () => import("@/views/Lookup/FormOutput"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/TransactionHistory",
    name: "TransactionHistory",
    props: true,
    component: () => import("@/views/VariousReports/TransactionHistory"),
    meta: {
      title: "화성시청",
      requireLogin: true,
    },
  },
  {
    path: "/Test",
    name: "Test",
    component: () => import("@/views/test"),
    meta: {
      title: "test",
      requireLogin: true,
    },
  },
  {
    path: "*",
    redirect: "/",
  },
];

const router = new VueRouter({
  mode: 'history',
  routes : route
})
router.afterEach(titleSetting);
// router.beforeEach(authCodeCheck);

export default router