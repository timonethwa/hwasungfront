import api from '@/api/axios';

const DepartmentManagement = {
    readDepartmentInfo: () => {
      return api.get("/departmentManagement/read");
    },
    createDepartmentInfo: (DepartmentInfo) => {
        return api.post("/departmentManagement/create", DepartmentInfo);
    },
    deleteDepartmentInfo: (DepartmentInfo) => {
        return api.post("/departmentManagement/delete", DepartmentInfo);
    },
    updateDepartmentInfo: (DepartmentInfo) => {
        return api.put("/departmentManagement/update", DepartmentInfo);
    },
};

export default DepartmentManagement;