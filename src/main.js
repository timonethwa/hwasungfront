import Vue from "vue";
import App from "./App.vue";
import router from "./routers";
import store from './store'
import axios from "axios";
// vuetify
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.prototype.$eventBus = new Vue();
Vue.prototype.$axios = axios;


export default new Vuetify({
  icons: {
    iconfont: 'mdi' // 'md' || 'mdi' || 'fa' || 'fa4' 
  }
})

Vue.use(Vuetify);

new Vue({
  el: "#app",
  router,
  store,
  components: { App },
  template: "<App/>",
  vuetify: new Vuetify(),
});
