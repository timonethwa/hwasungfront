import api  from "./axios";

const auths = {
  login: userInfo => {
      return api.post("/auths/login", userInfo);
    },
  logout: () => {
    return api.post("/auths/logout");
  },
  getUserInfo: () => {
    return api.get("/users/getUserInfo");
  },
  registUser: (userInfo) => {
    return api.put("/users/registUser", userInfo);
  },
};

export default auths;