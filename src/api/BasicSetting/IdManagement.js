import api from "@/api/axios";

const StaffStateColor = {
  readIdManagementInfo: () => {
    return api.get("/idManagement/read");
  },
  createIdManagementInfo: (IdManagementInfo) => {
    return api.post("/idManagement/create", IdManagementInfo);
  },
  deleteIdManagementInfo: (IdManagementInfo) => {
    return api.post("/idManagement/delete", IdManagementInfo);
  },
  updateIdManagementInfo: (IdManagementInfo) => {
    return api.put("/idManagement/update", IdManagementInfo);
  },
};

export default StaffStateColor;
