import api from '@/api';


const authStore = {
  namespaced: true,
  state: {
    userId: "",
    userName: "",
    token: "",
  },
  getters: {
    isLogin(state) {
      return state.token === "" ? false : true;
    },
    authToken(state) {
      // 0 이면 관리자 권환 1이면 읽기 권환
      return state.token === "0" ? false : true;
    },
  },
  mutations: {
    setUserId(state, userId) {
      state.userId = userId;
    },
    setToken(state, token) {
      state.token = token;
    },
    setUserName(state, userName) {
      state.userName = userName;
    },
    reset(state) {
      state.userId = "";
      state.token = "";
      state.userName = "";
    },
  },
  actions: {
    async login({ commit }, userInfo) {
      return api.auths
        .login(userInfo)
        .then((response) => {
          try {
            if (response.data[0].isSuccess == true) {
              // 권환을 위한 정보 저장
              commit("setUserId", response.data[0].userId);
              commit("setToken", response.data[0].authCode);
              commit("setUserName", response.data[0].userName);

              return true;
            } else {
              return false;
            }
          } catch (err) {
            return false;
          }
        })
        .catch((err) => {
          throw err;
        });
    },
    logout: ({ commit }) => {
      var res = false;
      try {
        commit("reset");
        res = true;
      } catch (err) {
        console.log(err);
      }
      return res;
    },
  },
};

export default authStore;
