import auths from "./auths";
import DepartmentManagement from './BasicSetting/DepartmentManagement';
import StaffStateColor from "./BasicSetting/StaffStateColor";
import IdManagement from "./BasicSetting/IdManagement";
import LoanSetting from "./BasicSetting/LoanSetting";
import StaffManagement from "./BasicSetting/StaffManagement";
import DepartmentHistory from "./BasicSetting/DepartmentHistory";
import FormOutput from "./Lookup/FormOutput";
import TransactionHistory from "./VariousReports/TransactionHistory";


const api = {
  auths,
  DepartmentManagement,
  StaffStateColor,
  IdManagement,
  LoanSetting,
  StaffManagement,
  DepartmentHistory,
  FormOutput,
  TransactionHistory,
};

export default api;
